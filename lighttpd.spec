%define webroot /var/www/lighttpd
%define confswitch() %{expand:%%{?with_%{1}:--with-%{1}}%%{!?with_%{1}:--without-%{1}}}
%bcond_without attr
%bcond_with    pcre
%bcond_without pcre2
%bcond_without nettle
%bcond_with    unwind
%bcond_without lua
%bcond_without brotli
%bcond_with    bzip2
%bcond_without zlib
%bcond_without zstd
%bcond_without maxminddb
%bcond_without dbi
%bcond_without ldap
%bcond_without mysql
%bcond_without pgsql
%bcond_without krb5
%bcond_without pam
%bcond_without sasl
%bcond_without gnutls
%bcond_with    mbedtls
%bcond_without nss
%bcond_without openssl
%bcond_without webdavprops
%bcond_without webdavlocks
%bcond_without tmpfiles
Summary:             Lightning fast webserver with light system requirements
Name:                lighttpd
Version:             1.4.77
Release:             1
License:             BSD-3-Clause and OML and GPLv3 and GPLv2
URL:                 https://github.com/lighttpd/lighttpd1.4
Source0:             http://download.lighttpd.net/lighttpd/releases-1.4.x/lighttpd-%{version}.tar.xz
Source1:             lighttpd.logrotate
Source2:             php.d-lighttpd.ini
Source3:             lighttpd.service
Patch0:              lighttpd-1.4.77-defaultconf.patch
Requires:            %{name}-filesystem system-logos
Requires(post):      systemd
Requires(preun):     systemd
Requires(postun):    systemd
BuildRequires:       systemd
Requires(post):      %{name}-mod_deflate
Requires(post):      %{name}-mod_webdav
%{?with_ldap:Requires(post): %{name}-mod_authn_ldap}
%{?with_ldap:Requires(post): %{name}-mod_vhostdb_ldap}
%{?with_lua:Requires(post): %{name}-mod_magnet}
%{?with_openssl:Requires(post): %{name}-mod_openssl}
Provides:            webserver
BuildRequires:       openssl-devel, pcre-devel, bzip2-devel, zlib-devel, autoconf, automake, libtool
BuildRequires:       /usr/bin/awk, libattr-devel, m4, pkg-config
%{?with_pcre:BuildRequires: pcre-devel}
%{?with_pcre2:BuildRequires: pcre2-devel}
%{?with_nettle:BuildRequires: nettle-devel}
%{?with_unwind:BuildRequires: libunwind-devel}

Provides: %{name}-mod_authn_mysql = %{version}-%{release}
Obsoletes: %{name}-mod_authn_mysql <= 1.4.63-1
 
Provides: %{name}-mod_mysql_vhost = %{version}-%{release}
Obsoletes: %{name}-mod_mysql_vhost <= 1.4.63-1

Provides: %{name}-mod_geoip = %{version}-%{release}
Obsoletes: %{name}-mod_geoip <= 1.4.63-1

%description
Secure, fast, compliant and very flexible web-server which has been optimized
for high-performance environments. It has a very low memory footprint compared
to other webservers and takes care of cpu-load. Its advanced feature-set
(FastCGI, CGI, Auth, Output-Compression, URL-Rewriting and many more) make
it the perfect webserver-software for every server that is suffering load
problems.

%package fastcgi
Summary:             FastCGI module and spawning helper for lighttpd and PHP configuration
Requires:            %{name} = %{version}-%{release} spawn-fcgi
%description fastcgi 
This package contains the spawn-fcgi helper for lighttpd's automatic spawning
of local FastCGI programs. Included is also a PHP .ini file to change a few
defaults needed for correct FastCGI behavior.

%if %{with dbi}
%package mod_authn_dbi
Summary:             Authentication module for lighttpd that uses DBI
Requires:            %{name} = %{version}-%{release}
%{?with_dbi:BuildRequires: libdbi-devel}
%{?with_dbi:Suggests: libdbi-dbd-mysql}
%{?with_dbi:Suggests: libdbi-dbd-pgsql}
%{?with_dbi:Suggests: libdbi-dbd-sqlite}

%description mod_authn_dbi
Authentication module for lighttpd that uses DBI
%endif

%if %{with krb5}
%package mod_authn_gssapi
Summary:             Authentication module for lighttpd that uses GSSAPI
Requires:            %{name} = %{version}-%{release}
%{?with_krb5:BuildRequires: krb5-devel}

%description mod_authn_gssapi
Authentication module for lighttpd that uses GSSAPI
%endif

%if %{with ldap}
%package mod_authn_ldap
Summary:             Authentication module for lighttpd that uses LDAP
Requires:            %{name} = %{version}-%{release}
%{?with_ldap:BuildRequires: openldap-devel}

%description mod_authn_ldap
Authentication module for lighttpd that uses LDAP
%endif

%if %{with pam}
%package mod_authn_pam
Summary:             Authentication module for lighttpd that uses PAM
Requires:            %{name} = %{version}-%{release}
%{?with_pam:BuildRequires: pam-devel}

%description mod_authn_pam
Authentication module for lighttpd that uses PAM.
%endif


%if %{with sasl}
%package mod_authn_sasl
Summary:             Authentication module for lighttpd that uses SASL
Requires:            %{name} = %{version}-%{release}
%{?with_sasl:BuildRequires: cyrus-sasl-devel}

%description mod_authn_sasl
Authentication module for lighttpd that uses SASL.
%endif


%package mod_deflate
Summary:             Compression module for lighttpd
Requires:            %{name} = %{version}-%{release}
%{?with_zlib:BuildRequires: zlib-devel}
%{?with_zstd:BuildRequires: libzstd-devel}
%{?with_bzip2:BuildRequires: bzip2-devel}
%{?with_brotli:BuildRequires: brotli-devel}

%description mod_deflate
Compression module for lighttpd.


%if %{with gnutls}
%package mod_gnutls
Summary:             TLS module for lighttpd that uses GnuTLS
Requires:            %{name} = %{version}-%{release}
%{?with_gnutls:BuildRequires: gnutls-devel}

%description mod_gnutls
TLS module for lighttpd that uses GnuTLS.
%endif


%if %{with lua}
%package mod_magnet
Summary:             Lua module for lighttpd
Requires:            %{name} = %{version}-%{release}
%{?with_lua:BuildRequires: lua-devel}

%description mod_magnet
Lua module for lighttpd.
%endif


%if %{with maxminddb}
%package mod_maxminddb
Summary:             GeoIP2 module for lighttpd to use for location lookups
Requires:            %{name} = %{version}-%{release}
%{?with_maxminddb:BuildRequires: libmaxminddb-devel}
%{?with_maxminddb:Recommends: GeoIP-GeoLite-data}
%{?with_maxminddb:Recommends: GeoIP-GeoLite-data-extra}
%{?with_maxminddb:Suggests: geoipupdate}
%{?with_maxminddb:Suggests: geoipupdate-cron}

%description mod_maxminddb
GeoIP2 module for lighttpd to use for location lookups.
%endif


%if %{with mbedtls}
%package mod_mbedtls
Summary:             TLS module for lighttpd that uses mbedTLS
Requires:            %{name} = %{version}-%{release}
%{?with_mbedtls:BuildRequires: mbedtls-devel}

%description mod_mbedtls
TLS module for lighttpd that uses mbedTLS.
%endif


%if %{with nss}
%package mod_nss
Summary:             TLS module for lighttpd that uses NSS
Requires:            %{name} = %{version}-%{release}
%{?with_nss:BuildRequires: nss-devel}

%description mod_nss
TLS module for lighttpd that uses NSS.
%endif


%if %{with openssl}
%package mod_openssl
Summary:             TLS module for lighttpd that uses OpenSSL
Requires:            %{name} = %{version}-%{release}
%{?with_openssl:BuildRequires: openssl-devel}

%description mod_openssl
TLS module for lighttpd that uses OpenSSL.
%endif


%if %{with dbi}
%package mod_vhostdb_dbi
Summary:             Virtual host module for lighttpd that uses DBI
Requires:            %{name} = %{version}-%{release}
%{?with_dbi:BuildRequires: libdbi-devel}
%{?with_dbi:Suggests: libdbi-dbd-mysql}
%{?with_dbi:Suggests: libdbi-dbd-pgsql}
%{?with_dbi:Suggests: libdbi-dbd-sqlite}

%description mod_vhostdb_dbi
Virtual host module for lighttpd that uses DBI.
%endif


%if %{with ldap}
%package mod_vhostdb_ldap
Summary:             Virtual host module for lighttpd that uses LDAP
Requires:            %{name} = %{version}-%{release}
%{?with_ldap:BuildRequires: openldap-devel}

%description mod_vhostdb_ldap
Virtual host module for lighttpd that uses LDAP.
%endif


%if %{with mysql}
%package mod_vhostdb_mysql
Summary:               Virtual host module for lighttpd that uses MySQL
Requires:              %{name} = %{version}-%{release}
%{?with_mysql:BuildRequires: mariadb-connector-c-devel}

%description mod_vhostdb_mysql
Virtual host module for lighttpd that uses MySQL.
%endif


%if %{with pgsql}
%package mod_vhostdb_pgsql
Summary:             Virtual host module for lighttpd that uses PostgreSQL
Requires:            %{name} = %{version}-%{release}
%{?with_pgsql:BuildRequires: libpq-devel}

%description mod_vhostdb_pgsql
Virtual host module for lighttpd that uses PostgreSQL.
%endif


%package mod_webdav
Summary:             WebDAV module for lighttpd
Requires:            %{name} = %{version}-%{release}
%{?with_webdavprops:BuildRequires: libxml2-devel}
%{?with_webdavprops:BuildRequires: sqlite-devel}
%{?with_webdavlocks:BuildRequires: libxml2-devel}
%{?with_webdavlocks:BuildRequires: sqlite-devel}

%description mod_webdav
WebDAV module for lighttpd.

%package filesystem
Summary:             The basic directory layout for lighttpd
BuildArch:           noarch
Requires(pre): /usr/sbin/useradd
%description filesystem
The lighttpd-filesystem package contains the basic directory layout
for the lighttpd server including the correct permissions
for the directories.

%prep
%setup -q
%patch -P 0 -b .defaultconf

%build
autoreconf -if
%configure \
    --libdir='%{_libdir}/lighttpd' \
    %{confswitch pcre} \
    %{confswitch pcre2} \
    %{confswitch nettle} \
    %{confswitch attr} \
    %{confswitch mysql} \
    %{confswitch pgsql} \
    %{confswitch dbi} \
    %{confswitch krb5} \
    %{confswitch ldap} \
    %{confswitch pam} \
    %{confswitch sasl} \
    %{confswitch gnutls} \
    %{confswitch mbedtls} \
    %{confswitch nss} \
    %{confswitch openssl} \
    %{?with_webdavprops:--with-webdav-props} \
    %{?with_webdavlocks:--with-webdav-locks} \
    %{?with_lua:--with-lua=lua} \
    %{confswitch zlib} \
    %{confswitch zstd} \
    %{confswitch bzip2} \
    %{confswitch brotli} \
    %{confswitch maxminddb} \
    %{confswitch unwind}
%make_build

%install
%make_install
install -D -p -m 0644 %{SOURCE1} \
    %{buildroot}%{_sysconfdir}/logrotate.d/lighttpd
install -D -p -m 0644 %{SOURCE2} \
    %{buildroot}%{_sysconfdir}/php.d/lighttpd.ini
install -D -p -m 0644 %{SOURCE3} \
    %{buildroot}%{_unitdir}/lighttpd.service
mkdir -p %{buildroot}%{webroot}
rm -rf config
cp -a doc/config config
find config -name 'Makefile*' | xargs rm -f
chmod -x doc/scripts/*.sh
mkdir -p %{buildroot}%{_sysconfdir}/lighttpd
cp -a config/*.conf config/*.d %{buildroot}%{_sysconfdir}/lighttpd/
mkdir -p %{buildroot}%{_var}/log/lighttpd
mkdir -p %{buildroot}%{_var}/run/lighttpd
%if %{with tmpfiles}
mkdir -p %{buildroot}/usr/lib/tmpfiles.d
echo 'D /run/lighttpd 0750 lighttpd lighttpd -' > \
    %{buildroot}/usr/lib/tmpfiles.d/lighttpd.conf
%endif
mkdir -p %{buildroot}%{_var}/lib/lighttpd/

%pre filesystem
/usr/sbin/useradd -s /sbin/nologin -M -r -d %{webroot} \
    -c 'lighttpd web server' lighttpd &>/dev/null || :

%post
%systemd_post lighttpd.service

%preun
%systemd_preun lighttpd.service

%postun
%systemd_postun_with_restart lighttpd.service


%files
%license COPYING
%doc AUTHORS README
%doc config/ doc/scripts/cert-staple.sh doc/scripts/rrdtool-graph.sh
%config(noreplace) %{_sysconfdir}/lighttpd/*.conf
%config(noreplace) %{_sysconfdir}/lighttpd/conf.d/*.conf
%exclude %{_sysconfdir}/lighttpd/conf.d/deflate.conf
%exclude %{_sysconfdir}/lighttpd/conf.d/fastcgi.conf
%exclude %{_sysconfdir}/lighttpd/conf.d/magnet.conf
%exclude %{_sysconfdir}/lighttpd/conf.d/webdav.conf
%config %{_sysconfdir}/lighttpd/conf.d/mod.template
%config %{_sysconfdir}/lighttpd/vhosts.d/vhosts.template
%config(noreplace) %{_sysconfdir}/logrotate.d/lighttpd
%{_unitdir}/lighttpd.service
%if %{with tmpfiles}
%config(noreplace) /usr/lib/tmpfiles.d/lighttpd.conf
%endif
%{_sbindir}/lighttpd
%{_sbindir}/lighttpd-angel
%{_libdir}/lighttpd/
%exclude %{_libdir}/lighttpd/mod_authn_dbi.so
%exclude %{_libdir}/lighttpd/mod_authn_gssapi.so
%exclude %{_libdir}/lighttpd/mod_authn_ldap.so
%exclude %{_libdir}/lighttpd/mod_authn_pam.so
%exclude %{_libdir}/lighttpd/mod_authn_sasl.so
%exclude %{_libdir}/lighttpd/mod_deflate.so
%exclude %{_libdir}/lighttpd/mod_gnutls.so
%exclude %{_libdir}/lighttpd/mod_magnet.so
%exclude %{_libdir}/lighttpd/mod_maxminddb.so
%exclude %{_libdir}/lighttpd/mod_openssl.so
%exclude %{_libdir}/lighttpd/mod_nss.so
%exclude %{_libdir}/lighttpd/mod_vhostdb_dbi.so
%exclude %{_libdir}/lighttpd/mod_vhostdb_ldap.so
%exclude %{_libdir}/lighttpd/mod_vhostdb_mysql.so
%exclude %{_libdir}/lighttpd/mod_vhostdb_pgsql.so
%{_mandir}/man8/lighttpd*8*

%files fastcgi
%doc doc/outdated/fastcgi*.txt
%config(noreplace) %{_sysconfdir}/php.d/lighttpd.ini
%config(noreplace) %{_sysconfdir}/lighttpd/conf.d/fastcgi.conf

%if %{with dbi}
%files mod_authn_dbi
%dir %{_libdir}/lighttpd/
%{_libdir}/lighttpd/mod_authn_dbi.so
%endif

%if %{with krb5}
%files mod_authn_gssapi
%dir %{_libdir}/lighttpd/
%{_libdir}/lighttpd/mod_authn_gssapi.so
%endif

%if %{with ldap}
%files mod_authn_ldap
%dir %{_libdir}/lighttpd/
%{_libdir}/lighttpd/mod_authn_ldap.so
%endif

%if %{with pam}
%files mod_authn_pam
%dir %{_libdir}/lighttpd/
%{_libdir}/lighttpd/mod_authn_pam.so
%endif

%if %{with sasl}
%files mod_authn_sasl
%dir %{_libdir}/lighttpd/
%{_libdir}/lighttpd/mod_authn_sasl.so
%endif

%files mod_deflate
%doc doc/outdated/compress.txt
%config(noreplace) %{_sysconfdir}/lighttpd/conf.d/deflate.conf
%dir %{_libdir}/lighttpd/
%{_libdir}/lighttpd/mod_deflate.so

%if %{with gnutls}
%files mod_gnutls
%dir %{_libdir}/lighttpd/
%{_libdir}/lighttpd/mod_gnutls.so
%endif

%if %{with lua}
%files mod_magnet
%doc doc/outdated/magnet.txt
%config(noreplace) %{_sysconfdir}/lighttpd/conf.d/magnet.conf
%dir %{_libdir}/lighttpd/
%{_libdir}/lighttpd/mod_magnet.so
%endif

%if %{with maxminddb}
%files mod_maxminddb
%dir %{_libdir}/lighttpd/
%{_libdir}/lighttpd/mod_maxminddb.so
%endif

%if %{with mbedtls}
%files mod_mbedtls
%dir %{_libdir}/lighttpd/
%{_libdir}/lighttpd/mod_mbedtls.so
%endif

%if %{with nss}
%files mod_nss
%dir %{_libdir}/lighttpd/
%{_libdir}/lighttpd/mod_nss.so
%endif

%if %{with openssl}
%files mod_openssl
%dir %{_libdir}/lighttpd/
%{_libdir}/lighttpd/mod_openssl.so
%endif

%if %{with dbi}
%files mod_vhostdb_dbi
%dir %{_libdir}/lighttpd/
%{_libdir}/lighttpd/mod_vhostdb_dbi.so
%endif

%if %{with ldap}
%files mod_vhostdb_ldap
%dir %{_libdir}/lighttpd/
%{_libdir}/lighttpd/mod_vhostdb_ldap.so
%endif

%if %{with mysql}
%files mod_vhostdb_mysql
%dir %{_libdir}/lighttpd/
%{_libdir}/lighttpd/mod_vhostdb_mysql.so
%endif

%if %{with pgsql}
%files mod_vhostdb_pgsql
%dir %{_libdir}/lighttpd/
%{_libdir}/lighttpd/mod_vhostdb_pgsql.so
%endif

%files mod_webdav
%doc doc/outdated/webdav.txt
%config(noreplace) %{_sysconfdir}/lighttpd/conf.d/webdav.conf
%dir %{_libdir}/lighttpd/
%{_libdir}/lighttpd/mod_webdav.so

%files filesystem
%dir %{_sysconfdir}/lighttpd/
%dir %{_sysconfdir}/lighttpd/conf.d/
%dir %{_sysconfdir}/lighttpd/vhosts.d/
%dir %{_var}/run/lighttpd/
%dir %{_var}/lib/lighttpd/
%if %{with tmpfiles}
%ghost %attr(0750, lighttpd, lighttpd) %{_var}/run/lighttpd/
%else
%attr(0750, lighttpd, lighttpd) %{_var}/run/lighttpd/
%endif
%attr(0750, lighttpd, lighttpd) %{_var}/lib/lighttpd/
%attr(0750, lighttpd, lighttpd) %{_var}/log/lighttpd/
%attr(0700, lighttpd, lighttpd) %dir %{webroot}/

%changelog
* Tue Jan 14 2025 yaoxin <1024769339@qq.com> - 1.4.77-1
- Update to 1.4.77:
  * stronger TLS defaults: MinProtocol TLSv1.3; experimental TLS ECH support
  * lighttpd TLS defaults: MinProtocol TLSv1.3 Other configurations are still supported, but are
    not the default. Previous default: MinProtocol TLSv1.2 Current default: MinProtocol TLSv1.3
  * lighttpd TLS defaults now limit TLSv1.3 Groups to the IANA “Recommended” set: “X25519:P-256:P-384:X448”
    (https://www.iana.org/assignments/tls-parameters/tls-parameters.xhtml#tls-parameters-8) Configure
    Groups/Curves using ssl.openssl.ssl-conf-cmd += (“Groups” => “…”)

* Fri Oct 27 2023 liyanan <liyanan61@h-parners.com> - 1.4.72-1
- Update to 1.4.72

* Mon May 29 2023 Jia Chao <jiachao2130@126.com> - 1.4.67-3
- Remove unsupport BuildRequires: gamin-devel, this pkg is dropped.

* Thu Feb 02 2023 xu_ping <xuping33@h-partners.com> - 1.4.67-2
- Add buildrequires krb5-devel to fix check configure error

* Wed Oct 12 2022 liangqifeng <liangqifeng@ncti-gba.cn> - 1.4.67-1
- update to 1.4.67 to fix CVE-2022-41556

* Tue Sep 13 2022 cenhuilin <cenhuilin@kylinos.cn> - 1.4.63-5
- Fix CVE-2022-37797

* Fri Mar 11 2022 baizhonggui <baizhonggui@huawei.com> - 1.4.63-4
- Modify var.state_dir path from /etc/lighttpd/lighttpd.conf in lighttpd-1.4.62-defaultconf.patch

* Sat Feb 19 2022 baizhonggui <baizhonggui@huawei.com> - 1.4.63-3
- Fix excuting systemctl start lighttpd.service failed

* Fri Jan 14 2022 yaoxin <yaoxin30@huawei.com> - 1.4.63-2
- Fix CVE-2022-22707

* Thu Jan 13 2022 liyanan <liyanan32@huawei.com> - 1.4.63-1
- update to 1.4.63

* Fri May 27 2022 liyanan <liyanan@h-partners.com> - 1.4.53-3
- Disable fam support as gamin is deprecated

* Fri Jan 14 2022 yaoxin <yaoxin30@huawei.com> - 1.4.53-2
- Fix CVE-2022-22707

* Fri Jan 8 2021 chengzihan <chengzihan2@huawei.com> - 1.4.53-1
- Package init

